(ns clj-helper.debian-test
  (:require [clojure.test :refer :all]
            [clojure.java.io :refer [make-parents resource]]
            [clojure.string :as s]
            [me.raynes.fs :refer [delete-dir executable? exists?]]
            [clj-helper.debian :refer :all]))

(deftest maven-tests
  (let [cwd (System/getProperty "user.dir")
        test-dir (str cwd "/output")
        project-dst (str test-dir "/project.clj")]
    (make-parents project-dst)
    (System/setProperty "user.dir" test-dir)
    (testing "maven output renders correctly"
      (let [user-data {:package-name "maven-package"}
            maven? true]
        (make-control! user-data maven?)
        (make-rules! user-data maven?)
        (make-poms! user-data maven?)
        (are [f] (exists? f)
          "output/debian/control"
          "output/debian/rules"
          "output/debian/maven-package.poms")
        (is (executable? "output/debian/rules"))
        (are [a b] (= (slurp (resource b)) (slurp a))
          "output/debian/control" "test/debian/control-maven"
          "output/debian/rules" "test/debian/rules-maven"
          "output/debian/maven-package.poms" "test/debian/poms-maven")))
    (delete-dir test-dir)
    (System/setProperty "user.dir" cwd)))

(deftest dep-tests
  (let [cwd (System/getProperty "user.dir")
        test-dir (str cwd "/output")
        project-dst (str test-dir "/project.clj")]
    (make-parents project-dst)
    (System/setProperty "user.dir" test-dir)
    (testing "dependencies render correctly"
      (let [user-data {:package-name "deps-package"
                       :dependencies ["libuseful-clojure"
                                      "libpomegranate-clojure"]}
            maven? false]
        (make-control! user-data maven?)
        (is (exists? "output/debian/control"))
        (are [a b] (= (slurp (resource b)) (slurp a))
          "output/debian/control" "test/debian/control-deps")))
    (delete-dir test-dir)
    (System/setProperty "user.dir" cwd)))
