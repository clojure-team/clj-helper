# 0.4.0

- Update Standards-Version.
- Move Vcs links to Debian GitLab (salsa).
- Add flag for maven builds (--maven).

# 0.3.0

- Autocomplete existing control and copyright data, if present.
- Fix classpath generation.

# 0.2.1

- Fix typo in doc-base.

# 0.2.0

- Improve output packaging, fixing various typos.
- Improve tests.

# 0.1.0

- Initial release.
- Helps users package Clojure projects with Debian Maven Helper.
